package javaeehello;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless(mappedName="messagesBean")
public class MessagesPersistBean implements MessagesPersistBeanRemote {
    
   public MessagesPersistBean(){
   }

   @PersistenceContext(unitName="javaeehelloworld_JavaEEHelloWorld_pom_1.0-SNAPSHOTPU")
   private EntityManager entityManager;         

   @Override
   public void addMessage(Message msg) {
      entityManager.persist(msg);
   }    

   @Override
   @SuppressWarnings("unchecked")
   public List<Message> getMessages() {
      return entityManager.createQuery("SELECT m FROM Message m ORDER BY m.datetime DESC").getResultList();
   }
}