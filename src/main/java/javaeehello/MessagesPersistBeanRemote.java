/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javaeehello;

import java.util.List;
import javax.ejb.Remote;

/**
 * Jde o Java rozhraní, které
 * @author Petr Ryšavý
 */
@Remote
public interface MessagesPersistBeanRemote
{
	public void addMessage(Message msg);

   public List<Message> getMessages();
}