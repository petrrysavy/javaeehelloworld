/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaeehello;

import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.validation.constraints.Pattern;

/**
 *
 * @author Petr Ryšavý
 */
@ManagedBean
@ApplicationScoped
public class MessageManagedBean
{
	@EJB
	private MessagesPersistBeanRemote messagesPersistBean;
//	@Pattern(regexp = "/\\w{0,64}/")
	private String author;
//	@Pattern(regexp = "/.{0,256}/")
	private String text;

	public MessageManagedBean()
	{
	}

	public String getAuthor()
	{
		return author;
	}

	public void setAuthor(String author)
	{
		this.author = author;
	}

	public String getText()
	{
		return text;
	}

	public void setText(String text)
	{
		this.text = text;
	}
	
	public String newPost()
	{
		Message msg = new Message();
		msg.setAuthor(author);
		msg.setBody(text);
		messagesPersistBean.addMessage(msg);
		return "posts";
	}
	
	public List<Message> getMessagesList()
	{
		return messagesPersistBean.getMessages();
	}

}
